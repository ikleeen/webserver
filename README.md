# Task 6 solution 
Web server for parsing XML from post request and convert data to CSV

### Configure
-------------
Configuration files are available in "priv" directory.

You can configure HTTP listening port, max resource value and syslog application identificator in "priv/sys.config" before run app. Configuration files also provided LAGER logger configuration. 
```sh
[
{lager, [
    {handlers, [
      {lager_console_backend, info},
      {lager_file_backend, [{file, "log/webserver.log"},  {level, error}]},
      {lager_file_backend, [{file, "log/webserver_console.log"}, {level, info}]}
    ]}
]},
 {webserver, [{http_port,8010}, {datafile, "priv/data.csv"}]}			
].
```
"priv/test.config" provides a test configuration. 


### Build
-------------
Get dependence from Git and compile the application:
```sh
    $ make all
```
Run common tests:
```sh
    $ make test
```
### Run
-------------
To run the application:
```sh
    $ make run
```
