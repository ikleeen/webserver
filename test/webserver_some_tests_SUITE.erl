-module(webserver_some_tests_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-include("test_xml.hrl").

-compile(export_all).

-define(TEST_REST_METHODS,  				
			[				
			{get, 400, "Bad Request"},         	
			{post, 406, "Not Acceptable"},
			{post, 406, "Not Acceptable"},  
			{head, 400, "Bad Request"}, 
			{options, 400, "Bad Request"},
			{put, 400, "Bad Request"},
			{delete, 400, "Bad Request"},
			{trace, 400, "Bad Request"}  	
			]).

all() -> [
	validate_http_methods,
	validate_parser_no_xml,
	validate_parser_xml_with_no_data,
	validate_parser_xml_with_valid_data,
	validate_parser_xml_with_valid_data2
	]. 

init_per_suite(Config) ->
  inets:start(), 	
  Config.

end_per_suite(Config) ->
  inets:stop(),
  Config.


init_per_testcase(_All, Config) ->
  webserver:start(),
  Config.

end_per_testcase(_All, Config) ->
  webserver:stop(),
  Config.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
get_url() -> "http://127.0.0.1:8010/capture".

request(put) ->
 httpc:request(put, {get_url(), [], [], []}, [], []);
request(post) ->
 httpc:request(post, {get_url(), [], [], []}, [], []);
request(Method) ->
 httpc:request(Method, {get_url(), []}, [], []).

validate_http_methods(_Config) ->
    Check = fun({Method, ValidCode, ValidMsg}) ->
			{ok, Reply} = request(Method),	
                        {{_, ReceiveCode, ReceiveMsg}, _, _} = Reply,
				
			?assertEqual(ValidCode, ReceiveCode),
			?assertEqual(ValidMsg, ReceiveMsg)
	   end,
   [Check(A) || A <- ?TEST_REST_METHODS],
 ok.



validate_parser_no_xml(_Config) -> %file not created but request has invalid data
   file:delete("../../data.csv"),
   httpc:request(post, {get_url(), [], [], "Test post not xml"}, [], []), 
   timer:sleep(100),	
   {error, enoent} = file:read_file("../../data.csv").

validate_parser_xml_with_no_data(_Config) -> %file not created but request has invalid data
   file:delete("../../data.csv"),	
   httpc:request(post, {get_url(), [], [], ?INVALID_XML}, [], []), 
   timer:sleep(100),	
   {error, enoent} = file:read_file("../../data.csv").

validate_parser_xml_with_valid_data(_Config) -> 
   file:delete("../../data.csv"),	
   httpc:request(post, {get_url(), [], [], ?VALID_XML1}, [], []), %valid request 
   timer:sleep(100),
   {ok, Binary1} = file:read_file("../../data.csv"),		%data is present  
   httpc:request(post, {get_url(), [], [], ?INVALID_XML}, [], []),  %invalid request
   timer:sleep(100),	
   {ok, Binary1} = file:read_file("../../data.csv"),
   httpc:request(post, {get_url(), [], [], ?VALID_XML1}, [], []),  %and again valid 
   timer:sleep(100),
   TwoBin = <<Binary1/binary, Binary1/binary>>,
   {ok, TwoBin} = file:read_file("../../data.csv").    %file contains x2 binary

validate_parser_xml_with_valid_data2(_Config) -> %xml VALID_XML2 has one valid element with PROD_NAME at first and GTIN at second 
   file:delete("../../data.csv"),
   httpc:request(post, {get_url(), [], [], ?VALID_XML2}, [], []), 
   timer:sleep(100),
   {ok, Binary1} = file:read_file("../../data.csv").	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
