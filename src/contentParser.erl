-module(contentParser).

-behaviour(gen_server).

-include_lib("xmerl/include/xmerl.hrl").

-export([start_link/0]).

%% gen_server callbacks
-export([parse/1]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-define(PGTIN, "PROD_COVER_GTIN").
-define(PNAME, "PROD_NAME").
-define(PDESC, "PROD_DESC").
-define(BONAME, "BRAND_OWNER_NAME").

-record(state, {fd = []}).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

parse(Data) ->
   gen_server:cast(?MODULE, {parse, Data}).

parse_node(#xmlElement{name=Name, content=Content}, File) when Name =:= 'BaseAttributeValues'->
            collect_children(Content, File),
            parse_children(Content, File);
parse_node(#xmlElement{name=_Name, attributes=_Attributes, content=Content}, File)->
            parse_children(Content, File);
parse_node(_, _File) ->ok.

parse_children([], _File) ->
    ok;
parse_children([N| MoreNodes], File) ->
    parse_node(N, File),
    parse_children(MoreNodes, File).

collect([ #xmlAttribute{name=_Name, value=Value} | MoreAttributes], {_K, V}) 
                                            when  (Value =:= ?PGTIN) or
                                                (Value =:= ?PNAME) or 
                                                (Value =:= ?PDESC) or
                                                (Value =:= ?BONAME)  -> 
                                                                                   collect(MoreAttributes, {Value, V}) ;
collect([ #xmlAttribute{name='value', value=Value} | MoreAttributes],  {K, _V}) -> collect(MoreAttributes, {K, Value} ) ;
collect([ _H | MoreAttributes],  L) -> collect(MoreAttributes, L ) ;
collect([], L) -> L.

save_csv(Map, IOF) -> case (dict:is_key(?PGTIN, Map)) and (dict:is_key(?PNAME, Map)) of
		true ->
			Print = fun(Key) ->
					case dict:find(Key, Map) of
					  {ok, Value} -> io:format(IOF,"~ts|",[Value]);
					  error -> io:format(IOF,"|",[])
					end
				end,
			[ Print(Key) ||Key <- [?PGTIN, ?PNAME, ?PDESC, ?BONAME]],
			io:format(IOF,"~n",[]);
		false -> ok
		end.
		

collect_children(Node, File) ->
    Attr = lists:filter(fun ({X, Y}) -> (X =/= nop) and (Y =/= nop) end, [collect(Attributes, {nop,nop}) || #xmlElement{name='value', attributes=Attributes} <- Node]), 
    lager:info("Attributes: ~p", [ Attr]),

    case file:open(File,[append, {encoding, unicode}]) of
	{ok,IOF} ->
	    save_csv(dict:from_list(Attr), IOF),
	    file:sync(IOF),
	    file:close(IOF);	
	{error,Reason} ->
		lager:error("~p can't open ~p file: ~p",[?MODULE, File, Reason])
    end.
    



init([]) ->
	File =  case application:get_env(webserver, datafile) of 
  		{ok, Val} ->  	lager:info("SUCCESS read env datafile value: ~p", [ Val]),
			Val;
  		_undefined -> lager:error("WARNING can't read env datafile value. Use data.csv"),
		     "data.csv"	
 	end,
	{ok, #state{fd = File}}.

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.


handle_cast({parse, Data}, #state{fd = F}) when is_binary(Data)  ->
      {Doc, _Misc} = xmerl_scan:string(binary_to_list(Data)),
      parse_node(Doc, F),
    {noreply, #state{fd = F}};
handle_cast({parse, Data}, #state{fd = F}) when is_list(Data) ->
      {Doc, _Misc} = xmerl_scan:string(Data),
      parse_node(Doc, F),
    {noreply, #state{fd = F}};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

