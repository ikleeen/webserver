-module(webserver_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-define(ACCEPTORS, 100).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
	Dispatch = cowboy_router:compile([
		{'_', [
			{"/capture", index_handler, []},
			{'_', notfound_handler, []}
		]}
	]),

 	Port = case application:get_env(webserver, http_port) of 
  		{ok, Val} ->  	lager:info("SUCCESS read http_port value: ~p", [ Val]),
			Val;
  		_undefined -> lager:error("WARNING can't read http_port value. Use 8008"),
		     8008	
 	end,
	case cowboy:start_http(http_listener, ?ACCEPTORS,[{port, Port}], [{env, [{dispatch, Dispatch}]}]) of 
		{ok, _} -> lager:info({info, "HTTP server successfully launched on http_port=~p", Port}),
			webserver_sup:start_link();
		{error, Reason} ->  syslog_worker:log({err, "HTTP server not launched"}),
			lager:error("HTTP server not launched. ~p ~p", [?MODULE, {error, Reason}]),
			{error, Reason}
	end.

	

stop(_State) ->
    ok.
