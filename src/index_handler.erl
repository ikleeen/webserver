-module(index_handler).
-behaviour(cowboy_http_handler).
%% Cowboy_http_handler callbacks
-export([
	init/3,
	handle/2,
	terminate/3
]).

init({tcp, http}, Req, _Opts) ->
    {ok, Req, undefined_state}.

handle(Req, State) ->
    {Method, Req2} = cowboy_req:method(Req),
    echo(Method, Req2, State).		

bad_request(Req, State) ->
	{ok, Req1} = cowboy_req:reply(400, [], [<<"Bad Request.">>], Req),
	{ok, Req1, State}.

empty_body(Req, State) ->
	{ok, Req1} = cowboy_req:reply(406, [], [<<"Empty body.">>], Req),
	{ok, Req1, State}.


echo(<<"POST">>, Req2, State) ->
	 case cowboy_req:has_body(Req2) of 
		true -> 
			{ok, Body, Req3} = cowboy_req:body(Req2, [{length, infinity}]),
				           contentParser:parse(Body),
			{ok, Req4} = cowboy_req:reply(200, [], [<<"Try convert.">>], Req3),
			{ok, Req4, State};
		_Not -> empty_body(Req2, State)
	end;
echo(Other, Req2, State) ->
	     lager:warning("~p received request on unavaliable method: ~p. Only POST method provided",[?MODULE, Other]),
            bad_request(Req2, State).

terminate(_Reason, _Req, _State) ->
    ok.

