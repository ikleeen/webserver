REBAR = `which rebar || echo ./rebar`

all: deps compile

deps:
	@( $(REBAR) get-deps )

compile: clean
	@( $(REBAR) compile )

clean:
	@( $(REBAR) clean )
	rm -f erl_crash.dump
	rm -rf log
	rm -f test/app.config
	rm -rf test/logs
	rm -rf test/data.csv
	rm -f test/webserver_some_tests_SUITE.beam

test:   compile
	( $(REBAR) skip_deps=true eunit )
	ln priv/test.config test/app.config
	@( $(REBAR) skip_deps=true ct )
	rm -f test/app.config

run:
	@( erl -sname webserver-node -pa ebin deps/*/ebin -config priv/sys.config -s webserver)

.PHONY: all deps compile clean test run
